//Soal 1
const luasLingk = (radius) => {
	let pi = 3.14;
	return (pi*radius*radius);
}
const kelLingk = (radius) => {
	let pi = 3.14;
	return (pi*radius*2);
}


console.log(luasLingk(5));
console.log(kelLingk(5));

//Soal 2
let kalimat = "";
let kata1 = "saya";
let kata2 = "adalah";
let kata3 = "seorang";
let kata4 = "frontend";
let kata5 = "developer";

kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
console.log(kalimat);

//Soal 3
const newFunction = function literal(firstName, lastName){
  var temp = {firstName,lastName,fullName(){
      		console.log(`${firstName} ${lastName}`); 
    	}
	};
  return temp;
};
//Driver Code 
newFunction("William", "Imoh").fullName() 

//Soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

//Soal 5
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west, ...east];
//Driver Code
console.log(combined)