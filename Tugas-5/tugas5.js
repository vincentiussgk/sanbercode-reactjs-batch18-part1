// Soal 1

function halo(){
	return "Halo Sanbers!";
}

console.log(halo());

// Soal 2

function kalikan(num1, num2){
	return (num1*num2);
}

var num1=12;
var num2=4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Soal 3
function introduce(name,age,address,hobby){
	return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

// Soal 4

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var object = {
	nama : arrayDaftarPeserta[0],
	"jenis kelamin" : arrayDaftarPeserta[1],
	hobi : arrayDaftarPeserta[2],
	"tahun lahir" : arrayDaftarPeserta[3]
}

// Soal 5
var arrayOfObject = [
{
	nama: "strawberry",
  	warna: "merah",
  	"ada bijinya": "tidak",
  	harga: 9000 
},
{
  	nama: "jeruk",
	warna: "oranye",
  	"ada bijinya": "ada",
  	harga: 8000
},
{
	nama: "Semangka",
	warna: "Hijau & Merah",
	"ada bijinya": "ada",
	harga: 10000
},
{
	nama: "Pisang",
  	warna: "Kuning",
  	"ada bijinya": "tidak",
  	harga: 5000
}];

console.log(arrayOfObject[0]);

// Soal 6
var dataFilm = [];
function PushToData(array,input1,input2,input3,input4){
	data = {};
	data["nama"] = input1;
	data["durasi"] = input2;
	data["genre"] = input3;
	data["tahun"] = input4;
	array.push(data);
}
PushToData(dataFilm,"Raiders of the Lost Ark", "2 jam", "Action", "1999");
PushToData(dataFilm,"Temple of Doom", "2.5 jam", "Adventure", "2001");
console.log(dataFilm);